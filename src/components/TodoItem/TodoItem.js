import React, { Component } from 'react';
import './TodoItem.css';
import PropTypes from 'prop-types';

class TodoItem extends Component {
    render() {
        const { item, onClick } = this.props;
        let className = 'TodoItem';
        if (item.isComplete) className += ' TodoItem-complete';
        return (
            <div onClick={onClick} className={className}>
                <p>{item.title}</p>
            </div>
        )
    }
}

TodoItem.propTypes = {
    item: PropTypes.shape({
        title: PropTypes.string,
        isComplete: PropTypes.bool
    })
}

export default TodoItem;