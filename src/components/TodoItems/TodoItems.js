import React, { Component } from 'react';
import './TodoItems.css';
import TodoItem from '../TodoItem/TodoItem';
import { Formik } from 'formik';
import * as Yup from 'yup'
import TextField from '@material-ui/core/TextField';

class TodoItems extends Component {
    constructor() {
        super();
        this.onClickItem = this.onClickItem.bind(this);
        this.enterKey = this.enterKey.bind(this);
        this.state = {
            todoList: [
                { title: 'Create new app', isComplete: true },
                { title: 'Research Compoent', isComplete: true },
                { title: 'Research props, state', isComplete: true },
                { title: 'Research handle event', isComplete: false },
            ]
        }
    }

    onClickItem(item) {
        return (() => {
            const { todoList } = this.state;
            this.setState({
                todoList: todoList.map(it => it !== item ? { ...it } : { title: item.title, isComplete: !item.isComplete })
            });
        })
    }


    enterKey(event) {
        if (event.keyCode === 13) {
            const value = event.target.value;
            this.setState({
                todoList: [
                    { title: value, isComplete: false },
                    ...this.state.todoList
                ]
            })
        }
    }

    render() {
        const { todoList } = this.state;
        return (
            <div className="TodoItems">
                <Formik
                    initialValues={{ addNew: '' }}
                    validationSchema={Yup.object({
                        addNew: Yup.string()
                            .min(3, 'To short')
                    })}
                >
                {formik => (
                    <form onSubmit={formik.handleSubmit}>
                        <TextField
                            id="addNew"
                            onKeyUp={this.enterKey}
                            placeholder='Add new item'
                            error={formik.touched.addNew && Boolean(formik.errors.addNew)}
                            helperText={formik.touched.addNew && formik.errors.addNew}
                            {...formik.getFieldProps('addNew')}
                        />
                    </form>
                )}
                </Formik>
                {
                    todoList.length ? todoList.map((item, index) => <TodoItem key={index} item={item} onClick={this.onClickItem(item)}></TodoItem>)
                        : 'Nothing here.'
                }
            </div>
        )
    }
}

export default TodoItems;